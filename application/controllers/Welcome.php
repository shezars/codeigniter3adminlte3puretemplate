<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
		@MCA - April 2022 - JDIH BACKEND PAS
		Lets make some art
	*/
	
	public function index()
	{
		$this->load->view('v_main');
	}
}
